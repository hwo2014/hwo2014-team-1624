using System;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Globalization;

public class Bot {
	public static void Main(string[] args) {
	    string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
		//string sPista = args[4];
		//double dThrottle = 1.0;
		//int iVuelta = 1;

		Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		//while(dThrottle>= 0.025){
			//Console.WriteLine("Corrida {0}, Throttle {1}",iVuelta,dThrottle);
			using(TcpClient client = new TcpClient(host, port)) {
				NetworkStream stream = client.GetStream();
				StreamReader reader = new StreamReader(stream);
				StreamWriter writer = new StreamWriter(stream);
				writer.AutoFlush = true;

				new Bot(reader, writer, new Join(botName, botKey)/*,dThrottle,iVuelta,sPista*/);
			}
		//	dThrottle -=0.025;
		//	iVuelta++;
		//}
	}

	private StreamWriter writer;

	Bot(StreamReader reader, StreamWriter writer, Join join/*,double dThrottle,int iVuelta,string sPista*/) {
		this.writer = writer;
		string line;
        List<Piece> Pista = new List<Piece>();
		List<Carro>[] Carros = new List<Carro>[10];
		Dictionary<string,int> PosCar = new Dictionary<string,int>();
		List<double> Distancias = new List<double>();
		bool bTurbo = false;
		int iPosCar = 0;
		int iLast;
		BotId botId = new BotId(join.name,join.key);
		DateTime dt;
		string sOri;
		double dDistFaltante;
		List<Carro> MyCar;
		turboAvailable turbo;
		string sMyCar = "\"" + join.name.Trim() + "\"";
		int iLaneTot=0;
		int iLineIni, iLineFini,iSigPos,iPosActual;
		double dThrottle = 0.0;
		double dThrottleMax = 0.3;
		double dLengthPiece = 0.0;
        int iPosNextBend;
		double dSpeed = 0.0;
		double dTopSafeSpeed = 0.0;
		int iTicks=0;
		bool bContTurbo = false;
		string sTemp ="";
		for(int i=0;i<10;i++){
			Carros[i] = new List<Carro>();
		}
		bool bCrash = false;
		int iTurboIni = -1;
		double dPenalizacion=0;
		//double dThrottleChoque = 0.0;
		
		//send(new createRace(botId,sPista,"apodaranch",1));
		//send(new joinRace(botId,4));
		send(join);

		while((line = reader.ReadLine()) != null) {
			iTicks++;
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
			switch(msg.msgType) {
				case "carPositions":
					try{
					
						//Guardar posicion de lo carros en este momento
						sOri = msg.data.ToString();
						dt = DateTime.Now;
						guardarPosicionesCarro(Carros,sOri,dt.ToString("hh.mm.ss.fff"),PosCar,Distancias);
						//termina  de guardas las posiciones
						
						iPosCar = PosCar[sMyCar];
						iLast = Carros[iPosCar].Count-1;
						MyCar = Carros[iPosCar];
						iPosActual = MyCar[iLast].PiecePosition.iPieceIndex;
						iSigPos = (MyCar[iLast].PiecePosition.iPieceIndex == Pista.Count-1)? 0 : MyCar[iLast].PiecePosition.iPieceIndex + 1;
						iLineIni = MyCar[iLast].PiecePosition.iEndLane;
						dLengthPiece = Pista[iPosActual].length; // ya se calcla cuando se guarda la pista
						iPosNextBend = obtenerNextBend(Pista,iPosActual,PieceType.Bend);
						dDistFaltante =calcularDistanciaACurva(Pista,Distancias,MyCar[iLast].PiecePosition.iPieceIndex,MyCar[iLast].PiecePosition.dInPieceDist);
						
						if(dThrottleMax == 0){
							bCrash = false;
						}
						
						if(iTicks%6 == 0){
							if(!bCrash){
								dThrottleMax += 0.005;
							}/*else{
								dThrottleMax += (dThrottleChoque-dThrottleMax)/2.0;
							}*/
						}
						
						if(dThrottleMax>1 || dThrottleMax < 0){
							dThrottleMax = 1.0;
						}
	
						
						//Calcular la velocidad actual
						if (iLast > 0){
							dSpeed = MyCar[iLast].GetCurrentSpeed(Distancias,MyCar[iLast-1]);
						}
						
						//calcular la velocidad maxima para no volcar
						dTopSafeSpeed =  MyCar[iLast].topSpeedInCurve (Pista);
						//calcular distancia a la proxima curva

						if (dSpeed > dTopSafeSpeed && dTopSafeSpeed > 0  && dDistFaltante < 200){
							dThrottle = 0.0;
						}else {
							dThrottle = dThrottleMax;
						}
						//Console.WriteLine ("Tick = " + iTicks + " Throttle = " + dThrottle + " DistFaltante =" + dDistFaltante);
						//Console.WriteLine ("DeltaD = " + MyCar[iLast].dDeltaD + " DeltaT = " + MyCar[iLast].dDeltaT );
						//Console.WriteLine ("Speed = " + dSpeed + " Top Safe Speed = " +dTopSafeSpeed);
						
						if(bContTurbo){
								send(new Throttle(dThrottle*0.25));
						}else if(Pista[iPosNextBend].angle > 0.0 && iLineIni != iLaneTot-1 && Pista[iSigPos].bSwitch && MyCar[iLast].PiecePosition.dInPieceDist > (dLengthPiece*0.9)){
								//Console.WriteLine("dLengthPiece {0} {1}",MyCar[iLast].PiecePosition.dInPieceDist,dLengthPiece*0.9);
								if (cambiarCarril(iLineIni, iLineIni+1) == 0)
								{
									send(new Throttle(dThrottle));
								}

						}else if (Pista[iPosNextBend].angle < 0.0 && iLineIni != 0 && Pista[iSigPos].bSwitch && MyCar[iLast].PiecePosition.dInPieceDist > (dLengthPiece*0.9)){
								//Console.WriteLine("dLengthPiece {0} {1}",MyCar[iLast].PiecePosition.dInPieceDist,dLengthPiece*0.9);
								if (cambiarCarril(iLineIni, iLineIni-1) == 0)
								{
									send(new Throttle(dThrottle));
								}

						}else if(buscarCarrosEstorbando(Carros,PosCar,sMyCar)){
								//Console.WriteLine("Hay un vehiculo estorbando {0} {1} {2}",Pista[iSigPos].bSwitch,MyCar[iLast].PiecePosition.dInPieceDist,(dLengthPiece*0.9));
							   if(Pista[iSigPos].bSwitch && MyCar[iLast].PiecePosition.dInPieceDist > (dLengthPiece*0.5)){
									//Console.WriteLine("Hare switch");
								
									if(iLineIni == iLaneTot-1){
										iLineFini = iLineIni - 1;
									}else{
										iLineFini = iLineIni + 1;
									}

									if(cambiarCarril(iLineIni,iLineFini)==0){
										send(new Throttle(dThrottle));
									}

							   }else{
								   send(new Throttle(dThrottle));
							   }
						}else if(bTurbo){
								//Comienza a calcular distancia a la siguiente curva si turbo esta activado
								//dDistFaltante = calcularDistanciaACurva(Pista, Distancias,iPosActual, MyCar[iLast].PiecePosition.dInPieceDist);
								if(dDistFaltante>400){
									send(new turbo("hit it"));
									Console.WriteLine("Hare Turbo");
									bTurbo = false;
									iTurboIni = iTicks;
								}else{
									send(new Throttle(dThrottle));
								}
						}else{
								send(new Throttle(dThrottle));
						}
					}catch{
						Console.WriteLine("Catch Principal");
						send(new Throttle(dThrottle));
					}

					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
                    generarPista(Pista,msg.data);
					calcularDistancias(Pista,Distancias);
					iLaneTot = obtenerNumLineas(msg.data.ToString());
					Console.WriteLine("Race init - Lanes {0}",iLaneTot);
					//Console.WriteLine("Se genero pista - Numero de piezas = {0}",Pista.Count);
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					//imprimirLog(Carros[iPosCar]/*,iVuelta*/,Distancias);
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Ping());
					break;
				case "crash":
					
					Id crasher;
					crasher = JsonConvert.DeserializeObject<Id>(msg.data.ToString());
					sTemp = sMyCar.Replace("\"","");
					if(crasher.name == sTemp){
						dPenalizacion++;
						//dThrottleChoque = dThrottleMax;
						dThrottleMax -= 0.025*Math.Pow(dPenalizacion,2);
						bCrash = true;
						Console.WriteLine("Choque: dThrolle {0}",dThrottle);
					}else{
						send(new Ping());
					}

					break;
				case "turboAvailable":
					try{
						bTurbo = false;
						turbo = JsonConvert.DeserializeObject<turboAvailable>(msg.data.ToString());
						/*Console.WriteLine("turboDurationMilliseconds: {0}, turboDurationTicks: {1},turboFactor: {2}",
						turbo.turboDurationMilliseconds,
						turbo.turboDurationTicks,
						turbo.turboFactor);*/
					}catch{
						Console.WriteLine("Turbo Available");
						bTurbo = false;
						send(new Ping());
					}
					break;
				case "turboEnd":
					try{
						sTemp = obtenerValor(msg.data.ToString(),"\"name\":");
						if(sTemp.Trim() == sMyCar){
							bContTurbo = false;
							iTurboIni=-1;
						}
					}catch{
						bContTurbo = false;
						Console.WriteLine("Turbo Ended");
						send(new Ping());
					}
					break;
				case "turboStart":
					try{
						sTemp = obtenerValor(msg.data.ToString(),"\"name\":");
						if(sTemp.Trim() == sMyCar){
							bContTurbo = true;
						}
					}catch{
						bContTurbo = false;
						Console.WriteLine("Turbo Ended");
						send(new Ping());
					}
					break;
			default:
				send(new Ping());
				break;
			}
		}
	}

	private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}

	public int obtenerNumLineas(string _sOri){
		string sTemp ="";
		int iPos;
		
		iPos = _sOri.IndexOf("\"lanes\":");
		if(iPos != -1){
			sTemp = _sOri.Substring(iPos);
			iPos = sTemp.LastIndexOf("\"index\":");
			if(iPos != -1){
				sTemp = sTemp.Substring(iPos);
				sTemp = obtenerValor(sTemp,"\"index\":");
				iPos = sTemp.IndexOf("}");
				if(iPos != -1){
					sTemp = sTemp.Substring(0,iPos);
					return Convert.ToInt32(sTemp.Trim())+1;
				}
			}
		}
		
		return -1;
	}
	
	/*
	public void obtenerLineas(lane[] _Lanes,string _data){
		string sTemp ="";
		int iPos;
		string sBusqueda = "\"lanes\": [";
		iPos = _data.IndexOf(sBusqueda);
		if(iPos != -1){
			sTemp = _data.Substring(iPos + sBusqueda.Length);
			iPos = sTemp.IndexOf("],");
			if(iPos != -1){
				sTemp = sTemp.Substring(0,iPos);
				_Lanes = JsonConvert.DeserializeObject<lane[10]>(sTemp);
			}
		}
		
		return;		
	}*/
	
	public bool buscarCarrosEstorbando(List<Carro>[] _Carros,Dictionary<string,int> _PosCar,string _MyCar){
		int iMyPiece, iEnemyPiece,iMyLane,iEnemyLane;
		double dMyDistance,dEnemyDistance;
		int iMyPos;
		List<Carro> CarPosition;
		int iLast;
		
		iMyPos = _PosCar[_MyCar];
		CarPosition = _Carros[iMyPos];
		iLast = CarPosition.Count-1;
		
		iMyPiece = CarPosition[iLast].PiecePosition.iPieceIndex;
		dMyDistance =  CarPosition[iLast].PiecePosition.dInPieceDist;
		iMyLane = CarPosition[iLast].PiecePosition.iEndLane;
		for(int iCont = 0;iCont<_PosCar.Count;iCont++){
			if(iMyPos != iCont){
				CarPosition = _Carros[iCont];
				iLast = CarPosition.Count-1;
				iEnemyPiece = CarPosition[iLast].PiecePosition.iPieceIndex;
				dEnemyDistance =  CarPosition[iLast].PiecePosition.dInPieceDist;
				iEnemyLane = CarPosition[iLast].PiecePosition.iEndLane;
				
				if(iMyPiece == iEnemyPiece && iEnemyLane == iMyLane && dMyDistance < dEnemyDistance){
					return true;
				}
			}
		}
		
		return false;
		
	}
	
	public int cambiarCarril(int iPosOri, int iPosDes){
		if(iPosOri < iPosDes){
           // Console.WriteLine("Switch - Right {0} {1}", iPosOri, iPosDes);
			send(new switchLane("Right"));
			return 1;
		}else if(iPosOri == iPosDes){
			return 0;
		}else{
           // Console.WriteLine("Switch - Left {0} {1}", iPosOri, iPosDes);
			send(new switchLane("Left"));
			return 1;
		}
	}
	
	public double calcularDistanciaACurva(List<Piece> _Pista,List<double> _Distancias,int iPosIni,double dDistAvanzada){
		double dDist = 0.0;
		int iNextBend = 0;
		int iCont = 0;
		try{
			if(_Pista[iPosIni].Tipo != PieceType.Bend){
				dDist = _Pista[iPosIni].length - dDistAvanzada;
				iNextBend = obtenerNextBend(_Pista,iPosIni,PieceType.Bend);

				if(iPosIni > iNextBend){
				
					for(iCont = iPosIni+1;iCont<_Pista.Count;iCont++){
						dDist += _Pista[iCont].length;
					}
					
					for(iCont=0;iCont<iNextBend;iCont++){
						dDist += _Pista[iCont].length;
					}
				}else{
					for(iCont=iPosIni+1;iCont<iNextBend;iCont++){
						dDist += _Pista[iCont].length;
					}
				}
			}
		}catch{
			Console.WriteLine("Trone: calcularDistanciaACurva");
		}
		return dDist;
		
	}
	
	private void calcularDistancias(List<Piece> _Pista,List<double> _Distancias){
		double dDistTot = 0.0;
		
		for(int i = 0; i<_Pista.Count;i++){
			if(_Pista[i].Tipo == PieceType.Bend){
				dDistTot += Math.Abs((2*Math.PI*_Pista[i].radius*_Pista[i].angle)/360);
			}else{
				dDistTot += _Pista[i].length;
			}
			
			_Distancias.Add(dDistTot);
		}
	}
	
    private void generarPista(List<Piece> _Pista,Object data){
		string sCadena;
		string sPieza;
		string sBusqueda;
		string sTemp;
		int iPos;
		bool _bSwitch;
		double _length;
		double _radius;
		double _angle;	
		PieceType _Tipo;
		
		Console.WriteLine("Generando Pista");
		sCadena =  data.ToString();
		sCadena = sCadena.Replace("\n","");
		sBusqueda = "\"pieces\": [";
		iPos = sCadena.IndexOf(sBusqueda);
		sCadena = sCadena.Substring(iPos + sBusqueda.Length);
		iPos = sCadena.IndexOf("]");
		sCadena = sCadena.Substring(1,iPos-1);//-1 x empezar a contar en 0, -| para restarle la posicion en la que iniciara la cadena
	
		iPos = sCadena.IndexOf("}");
		if(iPos == -1){
			Console.WriteLine("Error: iPos -1");
			return;
		}

		sPieza = sCadena.Substring(0,iPos+1);
		sCadena = sCadena.Substring(iPos+2); // para que empiece en el siguiente {
		while(sPieza != ""){
			sTemp = obtenerValor(sPieza,"\"length\":");
			if(sTemp != ""){
				_length =  Convert.ToDouble(sTemp.Trim());
			}else{
				_length = 0.0;
			}

			sTemp = obtenerValor(sPieza,"\"radius\":");
			if(sTemp != ""){
				_radius =  Convert.ToDouble(sTemp.Trim());
			}else{
				_radius = 0.0;
			}
			
			sTemp = obtenerValor(sPieza,"\"angle\":");
			if(sTemp != ""){
				_angle =  Convert.ToDouble(sTemp.Trim());
			}else{
				_angle = 0.0;
			}
			
			sTemp = obtenerValor(sPieza,"\"switch\":");
			if(sTemp.Trim() == "true"){			
				_bSwitch = true;
			}else{
				Console.WriteLine(sTemp);
				_bSwitch = false;
			}
			
			if(_radius != 0){
				_Tipo = PieceType.Bend;
			}else{
				_Tipo = PieceType.Straight;
			}

			if (_length == 0) { //Calculamos la longitud de la curva
				_length = (2 * Math.PI * _radius * _angle) / 360;
			}
			
			_Pista.Add(new Piece(_Tipo,_bSwitch,_length,_radius,_angle));
			iPos =sCadena.IndexOf("}");
			sPieza = sCadena.Substring(0,iPos+1);
			sCadena = sCadena.Substring(iPos+1);
			if(iPos == -1){
				sPieza = "";
				sCadena = "";
			}
		}
		return;
    }
	 
	//Regresa la posicion de la siguiente pista del tipo enviado
	public int obtenerNextBend(List<Piece> _Pista,int iActualPos,PieceType _Tipo){
		int iPosIni;
		if(iActualPos==_Pista.Count-1){
			iPosIni = 0;
		}else{
			iPosIni = iActualPos+1;
		}
		for(int i=iPosIni;i<_Pista.Count;i++){
			if(_Pista[i].Tipo == _Tipo){
				return i;
			}
		}
		
		for(int i = 0; i<iPosIni; i++){
			if(_Pista[i].Tipo == _Tipo){
				return i;
			}
		}
		
		return -1;
	}
	
	public string obtenerValor(string sPieza,string Atributo){
		string sTemp;
		int iPos1;
		int iPos2;
		iPos1 = sPieza.IndexOf(Atributo);
		if(iPos1 == -1){
			return "";
		}
		sTemp = sPieza.Substring(iPos1);
		sTemp = sTemp.Trim();
		iPos2 = sTemp.IndexOf(",");
		
		if(iPos2 == -1){
			iPos2 = sTemp.IndexOf("}");
			if(iPos2 == -1){
				iPos2 = sTemp.IndexOf("\n");
			}
		}
		
		sTemp = sTemp.Substring(0,iPos2);

		if(iPos1!=-1){
			return sTemp.Substring(Atributo.Length);
		}else{
			return "";
		}
	}
	
	public void guardarPosicion(List<Carro> _Carros,string sPosition,string time, List<double> Distancias){
		string Name="";
		string Color="";
		double Angle=0.0;
		Position PiecePosition;
		int lap =0;
		string sTemp="";
		int pieceIndex =-1,startLaneIndex=-1,endLaneIndex=-1;
		double inPieceDistance=-1;
		
		
		string sCopia = sPosition.Replace("}","");
		sCopia = sCopia.Replace("{","");
		
		Name = obtenerValor(sCopia,"\"name\":");
		Color= obtenerValor(sCopia,"\"color\":");


		sTemp = obtenerValor(sCopia,"\"angle\":");
		if(sTemp != ""){
			Angle = Convert.ToDouble(sTemp.Trim());
		}else{
			Angle = -1;
		}
		
		sTemp = obtenerValor(sCopia,"\"lap\":");
		if(sTemp != ""){
			lap = Convert.ToInt32(sTemp.Trim());
		}else{
			lap = -1;
		}

		sTemp = obtenerValor(sCopia,"\"pieceIndex\":");
		if(sTemp != ""){
			pieceIndex = Convert.ToInt32(sTemp.Trim());
		}else{
			pieceIndex = -1;
		}	
		
		sTemp = obtenerValor(sCopia,"\"inPieceDistance\":");
		if(sTemp != ""){
			inPieceDistance = Convert.ToDouble(sTemp.Trim());
		}else{
			inPieceDistance = -1;
		}	

		sTemp = obtenerValor(sCopia,"\"startLaneIndex\":");
		if(sTemp != ""){
			startLaneIndex = Convert.ToInt32(sTemp.Trim());
		}else{
			startLaneIndex = -1;
		}	

		sTemp = obtenerValor(sCopia,"\"endLaneIndex\":");
		if(sTemp != ""){
			endLaneIndex = Convert.ToInt32(sTemp.Trim());
		}else{
			endLaneIndex = -1;
		}			

		PiecePosition = new Position(pieceIndex,inPieceDistance,startLaneIndex,endLaneIndex);	
		
		double distanciaNueva = (PiecePosition.iPieceIndex > 0 ? Distancias [PiecePosition.iPieceIndex - 1] : 0) + PiecePosition.dInPieceDist;					 
		_Carros.Add(new Carro(Name,Color,Angle,PiecePosition,lap,time,distanciaNueva  ));
		return;
	}
	
	public void guardarPosicionesCarro(List<Carro>[] _Carros,string sPositions,string time,Dictionary<string,int> _NameCars, List<double> Distancias){
		string sPosCar,sBusqueda,sCar;
		int iPosIni,iPosFini,iPosCar;
		
		sBusqueda = "\"id\":";
		iPosIni = sPositions.IndexOf(sBusqueda);
		if(iPosIni == -1){
			return;
		}
		
		while(iPosIni != -1){
			sPositions = sPositions.Substring(iPosIni+sBusqueda.Length);
			iPosFini = sPositions.IndexOf(sBusqueda);
			if(iPosFini != -1){
				sPosCar = sPositions.Substring(0,iPosFini-1);
			}else{
				sPosCar = sPositions;
			}
			sCar = obtenerValor(sPosCar,"\"name\":");
			
			if(!_NameCars.ContainsKey(sCar.Trim())){
				_NameCars.Add(sCar.Trim(),_NameCars.Count);	
			}
			iPosCar = _NameCars[sCar.Trim()];
			guardarPosicion(_Carros[iPosCar],sPosCar,time,Distancias);
			
			if(iPosFini != -1){
				sPositions = sPositions.Substring(iPosFini);
				iPosIni = sPositions.IndexOf(sBusqueda);
			}else{
				return;
			}
		}
	}
	
	public void imprimirLog(List<Carro> _Carros/*,int iVuelta*/,List<double> _Distancias){
		string sNombre;
		double distTotal = 0.0;
		//Console.WriteLine("Guardando Log - Vuelta {0}...",iVuelta);
		sNombre = "LogCarrera"/* + iVuelta*/ + ".txt";
		using(StreamWriter sw = File.CreateText(sNombre)){
			sw.WriteLine("Angle|lap|PieceIndex|PieceDist|StartLane|EndLane|DistanciaTotal|Tiempo|");
			for(int i=0;i<_Carros.Count;i++){
				if(_Carros[i].PiecePosition.iPieceIndex-1<0){
					distTotal = _Carros[i].PiecePosition.dInPieceDist;
				}else{
					distTotal = _Distancias[_Carros[i].PiecePosition.iPieceIndex-1] + _Carros[i].PiecePosition.dInPieceDist;
				}
				
				sw.WriteLine("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|",
							_Carros[i].Angle,
							_Carros[i].lap,
							_Carros[i].PiecePosition.iPieceIndex,
							_Carros[i].PiecePosition.dInPieceDist,
							_Carros[i].PiecePosition.iStartLane,
							_Carros[i].PiecePosition.iEndLane,
							distTotal,
							_Carros[i].time);
			}
		}

        sNombre = "LogDistanciasPista.txt";
        using (StreamWriter sw = File.CreateText(sNombre)) {
            for (int i = 0; i < _Distancias.Count; i++){
                sw.WriteLine("Piece {0} - Distancias {1}", i, _Distancias[i]);
            }
        }
		Console.WriteLine("Log guardado.");
	}
}

class MsgWrapper {
    public string msgType;
    public Object data;

    public MsgWrapper(string msgType, Object data) {
    	this.msgType = msgType;
    	this.data = data;
    }
}

abstract class SendMsg {
	public string ToJson() {
		return JsonConvert.SerializeObject(new MsgWrapper(this.MsgType(), this.MsgData()));
	}
	protected virtual Object MsgData() {
        return this;
    }

    protected abstract string MsgType();
}
/*
class createRace: SendMsg{
}*/

class Join: SendMsg {
	public string name;
	public string key;
	public string color;

	public Join(string name, string key) {
		this.name = name;
		this.key = key;
		this.color = "red";
	}

	protected override string MsgType() { 
		return "join";
	}
}

class Ping: SendMsg {
	protected override string MsgType() {
		return "ping";
	}
}

class switchLane: SendMsg{
	public string value;
	
	public switchLane(string value){
		this.value = value;
	}
	
	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "switchLane";
	}	
}

class Throttle: SendMsg {
	public double value;

	public Throttle(double value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}

	protected override string MsgType() {
		return "throttle";
	}
}

public enum PieceType{
    Default_type=-1,
    Straight=1,
    Bridge =2,
    Bend =0
};

public class Piece{
    public PieceType Tipo;
    public bool bSwitch;
    public double length;
    public double radius;
    public double angle;

    public Piece(){
        Tipo = PieceType.Default_type;
        bSwitch = false;
        length = 0.0;
        radius = 0.0;
        angle = 0.0;
    }

    public Piece(PieceType _Tipo,bool _bSwitch,double _length,double _radius,double _angle){
        Tipo = _Tipo;
        bSwitch = _bSwitch;
        length = _length;
        radius = _radius;
        angle = _angle;
    }

}

public class Carro{
	public string Name;
	public string Color;
	public double Angle;
	public Position PiecePosition;
	public int lap;
	public double dThrottle;
	public string time;
	public double DistanciaRecorrida;
	public double Tiempo;
	public double dDeltaT;
	public double dDeltaD;
	
	public Carro(){
		Name = "";
		Color = "";
		Angle = 0.0;
		PiecePosition = new Position();
		lap = 0;
		time = "00:00:00";
		dThrottle = 0.0;
		DistanciaRecorrida = 0.0;
		Tiempo = DateTime.Now.Ticks * Math.Pow (10,-12) ; //Ticks es en nano segundos Se multiplica por 1000000 para convertirlo a milisegundos
	}
	
	public Carro(string _Name,string _Color,double _Angle,Position _PiecePosition,int _lap,string _time, double _DistanciaRecorrida){
		Name = _Name;
		Color = _Color;
		Angle = _Angle;
		lap = _lap;
		//dThrottle = _dThrottle;
		//PiecePosition = new Position();
		PiecePosition = _PiecePosition;
		time = _time;
		DistanciaRecorrida = _DistanciaRecorrida;
		Tiempo = DateTime.Now.Ticks * Math.Pow (10,-12);
	}
	public double GetCurrentSpeed(List<double> Distancias, Carro CarAnt)
	{
		double Speed = 0;
		double TiempoActual = DateTime.Now.Ticks * Math.Pow (10,-12);
		double distanciaNueva = (CarAnt.PiecePosition.iPieceIndex > 0 ? Distancias [CarAnt.PiecePosition.iPieceIndex - 1] : 0) + CarAnt.PiecePosition.dInPieceDist;
		double DeltaTime = (Math.Abs(TiempoActual)) - (Math.Abs( CarAnt.Tiempo));
		Speed =  (DistanciaRecorrida-distanciaNueva) / DeltaTime;
		dDeltaD = DistanciaRecorrida - distanciaNueva;
		dDeltaT = TiempoActual - Tiempo; 
		//Console.WriteLine ("tiempoActual = " + TiempoActual + " Tiempo = " + Math.Abs (Tiempo)) ;
		Tiempo = TiempoActual;
		return Speed;
	}
	public double topSpeedInCurve(List<Piece> Pista){
		double topSpeed = 0.0;
		double alpha = Pista[PiecePosition.iPieceIndex ].radius;
		double tetha = 55; //Maximo teorico
		int iNextBend = 0;
		if(Pista[PiecePosition.iPieceIndex].Tipo != PieceType.Bend){
			iNextBend = obtenerNextBend(Pista,PiecePosition.iPieceIndex,PieceType.Bend);
			alpha = Pista[iNextBend].radius;
		}
		topSpeed = Math.Tan (alpha)/Math.Tan (tetha);
		return topSpeed*1000000000000;
	}
	
	//Regresa la posicion de la siguiente pista del tipo enviado
	public int obtenerNextBend(List<Piece> _Pista,int iActualPos,PieceType _Tipo){
		int iPosIni;
		if(iActualPos==_Pista.Count-1){
			iPosIni = 0;
		}else{
			iPosIni = iActualPos+1;
		}
		for(int i=iPosIni;i<_Pista.Count;i++){
			if(_Pista[i].Tipo == _Tipo){
				return i;
			}
		}
		
		for(int i = 0; i<iPosIni; i++){
			if(_Pista[i].Tipo == _Tipo){
				return i;
			}
		}
		
		return -1;
	}
	
}

public class Position{
	public int iPieceIndex;
	public double dInPieceDist;
	public int iStartLane;
	public int iEndLane;
	
	public Position(){
		iPieceIndex = 0;
		dInPieceDist = 0;
		iStartLane = 0;
		iEndLane = 0;
	}
	
	public Position(int _iPieceIndex,double _dInPieceDist,int _iStartLane,int _iEndLane){
		iPieceIndex = _iPieceIndex;
		dInPieceDist = _dInPieceDist;
		iStartLane = _iStartLane;
		iEndLane = _iEndLane;
	}
	/*
	public static PiecePosition operator=(PiecePosition _PiecePosition){
		return new PiecePosition(_PiecePosition.iPieceIndex,_PiecePosition.dInPieceDist,_PiecePosition.iStartLane,_PiecePosition.iEndLane);
	}*/
}

class BotId: SendMsg {
	public string name;
	public string key;

	public BotId(string name,string key) {
		this.name = name;
		this.key = key;
	}

	protected override string MsgType() {
		return "botId";
	}
}

class createRace: SendMsg{
	public BotId botId;
	public string trackName;
	public string passWord;
	public int carCount;
	
	public createRace(BotId botId,string trackName,string passWord,int carCount) {
		this.botId = botId;
		this.trackName = trackName;
		this.passWord =  passWord;
		this.carCount = carCount;
	}

	protected override string MsgType() {
		return "createRace";
	}	
}

class joinRace: SendMsg{
	public BotId botId;
	public int carCount;
	
	public joinRace(BotId botId,int carCount) {
		this.botId = botId;
		this.carCount = carCount;
	}

	protected override string MsgType() {
		return "joinRace";
	}		
}
/*
  "turboDurationMilliseconds": 500.0,
  "turboDurationTicks": 30,
  "turboFactor": 3.0
*/
public class turboAvailable{
	public double turboDurationMilliseconds {get;set;}
	public int turboDurationTicks {get;set;}
	public double turboFactor {get;set;}
}

class turbo: SendMsg{
	public string value;
	
	public turbo(string value) {
		this.value = value;
	}

	protected override Object MsgData() {
		return this.value;
	}
	
	protected override string MsgType() {
		return "turbo";
	}	
}

public class Track{
	public string id {get;set;}
	public string name {get;set;}
	public List<piece> pieces {get;set;}
	public List<lane> lanes {get;set;}
	public StartingPoint startingPoint {get;set;}
}


public class race{
	public Track track {get;set;}
	public List<car> cars {get;set;}
	public RaceSession raceSession {get;set;}
}

public class piece{
	public double length {get;set;}
	public bool Switch {get;set;}
	public int radius {get;set;}
	public double angle {get;set;}
}

public class lane{
	public int distanceFromCenter {get;set;}
	public int index {get;set;}
}

public class position{
	public double x {get;set;}
	public double y {get;set;}
}

public class StartingPoint{
	public Position position {get;set;}
	public double angle {get;set;}
}

public class car{
	public Id id {get;set;}
	public Dimensions dimensions {get;set;}
}

public class Dimensions{
	public double length {get;set;}
	public double width {get;set;}
	public double guideFlagPosition {get;set;}
}

public class Id{
	public string name {get;set;}
	public string color {get;set;}
}

public class RaceSession{
	public int laps {get;set;}
	public int maxLapTimeMs {get;set;}
	public bool quickRace {get;set;}
}